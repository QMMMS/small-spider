import requests
import bs4
import os
import datetime

base_index_url	= 'https://www.miffv.com/gushi/'
base_net_url	= 'https://www.miffv.com/'
name_and_herf={}

def download_pic(src, name, extend_name):
    # print(src)
    if not os.path.exists("E:/Pic/"):
        os.mkdir("E:/Pic/")

    root = "E:/Pic/chengyu/"
    if not os.path.exists(root):
        os.mkdir(root)
    path = root + name + '.' + extend_name
    print(datetime.datetime.now(), end=":")
    if src is not None:
        if not os.path.exists(path):
            try:
                with open(path, "wb") as f:
                    f.write(requests.get(src, timeout=1).content)
                    f.close()
                    print(path + "图片保存成功!")
            except:
                print(path + "图片保存失败!")
        else:
            print(path + "图片已存在!")
    else:
        print(path + "好像无法加载，自动跳过....")
        pass

for i in range(1,21):
    index_url = base_index_url + str(i) + '.html'
    r = requests.get(index_url)
    result = r.text.encode('ISO-8859-1').decode('utf-8')

    soup = bs4.BeautifulSoup(result, 'html.parser')
    ul = soup.find(name="ul", attrs={"class" :"ulLi120"})
    lis = ul.findAll(name="li")
    for li in lis:
        a = li.find(name="a")
        name_and_herf[a.text] = a.get("href")


for name, herf in name_and_herf.items():
    url = base_index_url + herf
    r = requests.get(url)
    result = r.text.encode('ISO-8859-1').decode('utf-8')
    soup = bs4.BeautifulSoup(result, 'html.parser')
    div = soup.find(name="div", attrs={"class" :"boxHui content"})
    img = div.find(name="img")
    src = img.get("src")
    extend_name = src.split(".")[-1]
    src = src[3:]
    src = base_net_url + src
    download_pic(src, name, extend_name)
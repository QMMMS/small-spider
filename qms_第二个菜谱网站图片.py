import requests
import bs4
import os
import datetime

base_url = 'https://www.xinshipu.com/caipu/404/?page='


def get_html_by_requests(url):
    try:
        headers = {
            "Accept-Encoding": "gzip, deflate",
            "Accept-Language": "zh-CN,zh;q=0.9",
            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) "
                          "AppleWebKit/537.36 (KHTML, like Gecko) "
                          "Chrome/92.0.4515.107 Safari/537.36 "
        }
        return requests.get(url, headers=headers, timeout=1)
    except:
        return None


def download_pic(src, name, extend_name):
    if not os.path.exists("E:/Pic/"):
        os.mkdir("E:/Pic/")
    
    root = "E:/Pic/recipe/"
    if not os.path.exists(root):
        os.mkdir(root)

    path = root + name + '.' + extend_name
    read = get_html_by_requests(src)
    print(datetime.datetime.now(), end=":")
    if read is not None:
        if not os.path.exists(path):
            try:
                with open(path, "wb") as f:
                    f.write(read.content)
                    f.close()
                    print(path + "图片保存成功!")
            except:
                print(path + "图片保存失败!")
        else:
            print(path + "图片已存在!")
    else:
        print(path + "好像无法加载，自动跳过....")
        pass


def request_url(url):
    try:
        headers = {"User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) "
                                 "AppleWebKit/537.36 (KHTML, like Gecko) "
                                 "Chrome/92.0.4515.107 Safari/537.36"}
        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            return response
    except requests.RequestException:
        return None

for i in range(1,48):
    url = base_url + str(i)
    r = request_url(url)
    r_text = r.text
    soup=bs4.BeautifulSoup(r_text,'html.parser')
    v_pw_div = soup.find_all('div',class_='v-pw')
    for v_pw in v_pw_div:
        #print(v_pw)
        src = 'http:'+v_pw.find('img')['src']
        name = v_pw.find('img')['alt']
        extend_name = src.split('.')[-1]
        print(i,end=':')
        download_pic(src, name, extend_name)
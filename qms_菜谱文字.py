import requests
import bs4
import os
import datetime

base_index_url	= 'https://home.meishichina.com/recipe/tanggeng/page/'

def request_url(url):
    try:
        headers = {"User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) "
                                 "AppleWebKit/537.36 (KHTML, like Gecko) "
                                 "Chrome/92.0.4515.107 Safari/537.36"}
        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            return response
    except requests.RequestException:
        return None


def get_html_by_requests(url):
    try:
        headers = {
            "Accept-Encoding": "gzip, deflate",
            "Accept-Language": "zh-CN,zh;q=0.9",
            "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) "
                          "AppleWebKit/537.36 (KHTML, like Gecko) "
                          "Chrome/92.0.4515.107 Safari/537.36 "
        }
        return requests.get(url, headers=headers, timeout=1)
    except:
        return None


def download_txt(text, name):
    if not os.path.exists("E:/Pic/"):
        os.mkdir("E:/Pic/")
    
    root = "E:/Pic/recipe_txt/"
    if not os.path.exists(root):
        os.mkdir(root)

    path = root + name + '.txt'
    print(datetime.datetime.now(), end=":")
    if text is not None:
        if not os.path.exists(path):
            try:
                with open(path, "wb") as f:
                    f.write(text)
                    f.close()
                    print(path + "菜谱保存成功!")
            except:
                print(path + "菜谱保存失败!")
        else:
            print(path + "菜谱已存在!")
    else:
        print(path + "好像无法加载，自动跳过....")
        pass

for i in range(1,201):
    try: 
        index_url = base_index_url + str(i) + '/'
        r=request_url(index_url)
        r_text = r.text
        if(r.encoding == 'ISO-8859-1'):
            r_text = r_text.encode('ISO-8859-1').decode('utf-8')
        soup=bs4.BeautifulSoup(r_text,'html.parser')
        div = soup.find('div',class_='ui_newlist_1 get_num')
        detail_div = div.find_all('div',class_='detail')
    except:
        continue

    if detail_div is None:
        continue

    for detail in detail_div:
        try: 
            h2 = detail.find('h2')
            a = h2.find('a')
            name = a.text
            href = a['href']
            # print(name + ' ' + href)
            r=request_url(href)
            r_text = r.text
            if(r.encoding == 'ISO-8859-1'):
                r_text = r_text.encode('ISO-8859-1').decode('utf-8')
            soup=bs4.BeautifulSoup(r_text,'html.parser')
            recipeStep_div = soup.find('div',class_='recipeStep')
            all_recipeStep_word_div = recipeStep_div.find_all('div',class_='recipeStep_word')

            text = ''
            for recipeStep_word_div in all_recipeStep_word_div:
                text = text + recipeStep_word_div.text
            print(i , end=":")
            download_txt(text.encode('utf-8'), name)
        except:
            continue

